
<!-- README.md -->

<!-- SPDX-FileCopyrightText: 2022 Alexander Murphy <supernova@alexmurphy.uk> -->
<!-- -->
<!-- SPDX-License-Identifier: CC-BY-4.0 -->

# Math Practice

A collection of [Clojure](https://clojure.org/) scripts (not [Clojurescript](https://clojurescript.org/)s!) containing solutions to the first 100 problems (eventually) of the [Project Euler](https://projecteuler.net/) question set.

## Notes

The derived solutions are not generalised, for example, if a question asks about numbers up to 1000, the solution will only work for that specific set of numbers (not 999, 1001, 5, 5000000, etc.).

Code is usually not time-elapsed or memory-usage optimised, at least not beyond that which is sufficient to solve the solution in a reasonable time frame (obviously this does not mean that all questions are simply 'brute-forced'). 

Interesting uses for features of the Clojure programming language have been marked with comments. This is more a personal academic exercise than any form of recommendation.

## Project Euler

tbc.

## Licence

tbc.

## Contact

tbc.

