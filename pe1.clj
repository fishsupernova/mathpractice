
; pe1.clj

; SPDX-FileCopyrightText: 2022 Alexander Murphy <supernova@alexmurphy.uk>
;
; SPDX-License-Identifier: EPL-2.0


;;; snippet start ;;;
; 
; SPDX-FileCopyrightText: Project Euler https://projecteuler.net/problem=1
; SPDX-License-Identifier: CC-BY-NC-4.0
; 
; If we list all the natural numbers below 10 that are multiples of 3 or 5, we 
; get 3, 5, 6 and 9. The sum of these multiples is 23.
; 
; Find the sum of all the multiples of 3 or 5 below 1000.
; 
;;; snippet end ;;;


(->> (range 1 1000) ; range is exclusive of 1000 here
     (filter #(or (= (rem % 5) 0) (= (rem % 3) 0)) ,,,) 
     (apply + ,,,)) ; commas are whitespace, but a nice visual aid

