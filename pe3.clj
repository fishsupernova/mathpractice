
; pe3.clj

; SPDX-FileCopyrightText: 2022 Alexander Murphy <supernova@alexmurphy.uk>
;
; SPDX-License-Identifier: EPL-2.0


;;; snippet start ;;;
; 
; SPDX-FileCopyrightText: Project Euler https://projecteuler.net/problem=3
; SPDX-License-Identifier: CC-BY-NC-4.0
; 
; The prime factors of 13195 are 5, 7, 13 and 29.
; 
; What is the largest prime factor of the number 600851475143?
; 
;;; snippet end ;;;


(def number 600851475143)


(defn remzero?
  "If the remainder of dividing the 'number' by m is zero, m is a factor (and 
  must be prime because value are checked incrementally) and is returned."
  [numrtr dvsr]
  (if (= 0 (rem numrtr dvsr)) numrtr))


(loop [n number
       m 2]
  (if (= n 1)
    (- m 1)
    (if (remzero? n m)
      (recur (/ n m) (inc m))
      (recur n (inc m)))))

; for posterity, wrote the below before I realised that the loop on line 31 
; was already returning primes

;(defn isprime? [n l] 
;  "Given a list of primes, l, if n=max(l)+1, and n can't be divided by any l, 
;  then it must be prime"
;  (if (empty? l)
;    true
;    (if (= (rem n (peek l)) 0)
;      false
;      (isprime? n (pop l)))))

